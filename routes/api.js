var Image    = require('../models/image');
var Category = require('../models/category');
var mongoose = require('mongoose');

module.exports = function(app, express){
  var api = express.Router();

  api.get('/categories', function(req, res) {
    Category.find({}, function(err, categories) {
        if(err) return res.json({ success: false, msg: "Something went wrong retrieving Categories", error: err });
        res.status(200).send({ success: true, result: categories });
    })
  });

  api.post('/categories', function(req, res) {
    var category = new Category({
			title: req.body.params.title
		});
    category.save(function(err) {
      if(err) return res.status(400).json({ success: false, msg: "Something went wrong creating the collection", error: err });
      res.status(201).send({success: true, msg: "Collection created!"});
    })
  });

  api.delete('/categories/:id', function(req, res) {
    var categoryId = req.params.id;
    if(!categoryId || categoryId === '') return res.json({success: false, msg: "A category id must be provided", error: err});
    Category.findByIdAndRemove(categoryId, function(err, removed){
        if(err) res.json({success: false, msg: "Something went wrong deleting the category. Try again.", error: err});
        res.status(200).json({success: true, msg: "Category deleted"});
    });
  });

  api.get('/category/:id', function(req, res) {
    var categoryId = req.params.id;
    Category.findById(categoryId, function(err, category) {
        if(err) return res.json({ success: false, msg: "Something went wrong retrieving the category", error: err });
        if(category) {
          Image.find()
          .where({
            category: categoryId
          })
          .exec(function(err,images){
            if(err) return res.json({ success: false, msg: "Something went wrong retrieving images", error: err });
            Category.populate(images, {path: "category"},function(err, comments){
              res.status(200).send({success: true, result: images});
            });
          });
        }else {
          res.status(404).send({success: false, msg: "Category doesn't exists"});
        }
    })
  });

  api.get('/images', function(req, res) {
    Image.find({}, function(err, images) {
        if(err) return res.json({ success: false, msg: "Something went wrong retrieving Images", error: err });
        res.status(200).send({ success: true, result: images });
    });
  });

  api.post('/images', function(req, res) {
    var image = new Image({
			imageBase64: req.body.params.imageBase64,
      category: mongoose.Types.ObjectId(req.body.params.category),
      geoLocation: JSON.stringify(req.body.params.location)
		});

    image.save(function(err) {
      if(err) return res.status(400).json({ success: false, msg: "Something went wrong creating the image", error: err });
      res.status(201).send({success: true, msg: "Image created!"});
    })
  });

  api.get('/images/:category', function(req, res) {
    Image.find({ category: mongoose.Types.ObjectId(req.params.category)}, function(err,images){
        if(err) return res.status(400).json({ success: false, msg: "Something went wrong retrieving images", error: err });
        res.status(200).send({success: true, result: images});
    });
  });

  return api;
};

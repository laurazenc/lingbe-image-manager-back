var path = require('path');
var rootPath = path.normalize(__dirname);
module.exports = {
    development:{
        rootPath: rootPath,
        db: "mongodb://localhost:27017/lingbe_image",
        port: process.env.PORT || 8080		
	}
}

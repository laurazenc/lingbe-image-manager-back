var express      = require('express'),
    morgan       = require('morgan'),
    bodyParser   = require('body-parser'),
    cors         = require('cors'),
    errorhandler = require('errorhandler'),
    helmet       = require('helmet');

module.exports = function(app, config, env){
    if(env === 'development') {
        app.use(morgan('dev'));
        app.use(errorhandler());
        app.use(cors());
        app.use(function(req, res, next) {
          res.set('Access-Control-Allow-Origin', '*');
          res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
          res.set('Access-Control-Allow-Headers', 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token');

          next();
        });
    }

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true}));
}

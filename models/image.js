var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ImageSchema = new Schema({
	category: { type: Schema.Types.ObjectId, ref: 'Category', required: true},
	imageBase64: { type: String, required: true },
	geoLocation: { type: String, required: true },
	created_at: { type: Date, defauly: Date.now},
  updated_at: { type: Date, defauly: Date.now}
});

ImageSchema.pre('save', function(next){
	now = new Date();
	this.updated_at = now;
	if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
});

module.exports = mongoose.model('Image', ImageSchema);

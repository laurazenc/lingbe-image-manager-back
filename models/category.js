var mongoose = require('mongoose'),
    Schema    = mongoose.Schema,
    ImageSchema = require('./image');

var CategorySchema = new Schema({
    title: {type: String, trim: true, required: true},
    created_at: { type: Date, defauly: Date.now},
    updated_at: { type: Date, defauly: Date.now}
});


CategorySchema.pre('save', function(next){
  now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
});

CategorySchema.pre('remove', function (next) {
	ImageSchema.remove({category: this._id}).exec();
    next();
});

module.exports = mongoose.model('Category', CategorySchema);
